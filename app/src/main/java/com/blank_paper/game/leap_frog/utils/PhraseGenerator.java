package com.blank_paper.game.leap_frog.utils;

import com.blank_paper.game.leap_frog.object.Word;

import java.util.ArrayList;
import java.util.Random;

/**
 * Created by walkmn on 29.07.15.
 */
public class PhraseGenerator {

    private ArrayList<String> phrases = new ArrayList<>();

    public PhraseGenerator(Word word) {
        phrases.add("Не правильно( Попробуй еще!");
        phrases.add("Не плохая попытка! Но нет(");
        phrases.add("Хм... Возможно стоит взять подсказку?");
        phrases.add("Все буквы на месте, только они не в том порядке стоят)");
        phrases.add("Ахаха...смешно(");
        phrases.add("В среднем люди отгадывают это слово за " + word.getSuperTime() * 1.5 + "секунд.");
        phrases.add("Вторая буква явно - \'" + word.getWord().charAt(1) + "\', не так ли?");
        phrases.add("Стоп, а может \"" + new WordMixer().mix(word.getWord()) + "\"?");
        phrases.add("В этом слове " + Math.round(Math.pow(word.getWord().length(), word.getWord().length())) + " вариантов. Но теперь на один меньше)");
    }

    public String generate() {
        return phrases.get(new Random().nextInt(phrases.size()));
    }
}
