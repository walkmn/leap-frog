package com.blank_paper.game.leap_frog.controllers;

import android.animation.ArgbEvaluator;
import android.animation.ObjectAnimator;
import android.animation.ValueAnimator;
import android.app.Activity;
import android.graphics.Color;
import android.os.AsyncTask;
import android.view.View;
import android.view.animation.Animation;
import android.view.animation.AnimationUtils;
import android.widget.LinearLayout;
import android.widget.TextView;
import android.widget.Toast;

import com.blank_paper.game.leap_frog.R;
import com.blank_paper.game.leap_frog.object.Word;
import com.blank_paper.game.leap_frog.utils.StorageManager;

/**
 * Created by walkmn on 29.07.15.
 */
public class TimerController {

    private Activity activity;
    private Word word;
    private ValueAnimator mColorAnimation;
    private long mCurrentPlayTime;

    public int getSolvedTime() {
        return timeSource - timeRemaining;
    }

    public int getTimeRemaining() {
        return timeRemaining;
    }

    private int timeRemaining;
    private LinearLayout llTimerProgressbar, ll_timer_clock;
    private TextView tvTimerClock;

    private TimerAsyncTask timerAsyncTask;
    private StorageManager storageManager;
    private int timeSource;

    TimerController(View view, Activity activity) {
        this.activity = activity;
        storageManager = new StorageManager(activity);

        llTimerProgressbar = (LinearLayout) view.findViewById(R.id.ll_timer_progressbar);
        tvTimerClock = (TextView) view.findViewById(R.id.tv_timer_clock);
        ll_timer_clock = (LinearLayout) view.findViewById(R.id.ll_timer_clock);
    }
    public void start(Word word) {

        int time = word.getSuperTime() * 3;
        this.word = word;
        this.timeSource = time;

        if (word.onPaused()) { // is not new word
            this.timeRemaining = storageManager.getTimeRemaining();
            mCurrentPlayTime = storageManager.getCurrentPlayTime();
        } else {
            this.timeRemaining = time;
            mCurrentPlayTime = 0;
        }

        startMainAnimation();
        resume();
    }

    public void stop() {
        mCurrentPlayTime = (long) ((Math.floor(mColorAnimation.getCurrentPlayTime()) / 1000) - 0.5) * 1000;
        storageManager.saveCurrentPlayTime(mCurrentPlayTime);
        storageManager.saveTimeRemaining(timeRemaining);
        mColorAnimation.cancel();

        if (timerAsyncTask != null) {
            timerAsyncTask.cancel(true);
        }
        hideProgressBar();
        hideClock();
    }

    public void resume() {
        timerAsyncTask = new TimerAsyncTask();
        timerAsyncTask.execute();
    }

    private void showTime() {
        activity.runOnUiThread(new Runnable() {
            @Override
            public void run() {
                tvTimerClock.setText(String.format("%02d", timeRemaining / 60) + ":" + String.format("%02d", timeRemaining % 60));
            }
        });
    }

    private void startMainAnimation() {
        Integer colorFrom = Color.parseColor("#2d851c");
        Integer colorTo = Color.parseColor("#FF0000");
        mColorAnimation = ValueAnimator.ofObject(new ArgbEvaluator(), colorFrom, colorTo);
        mColorAnimation.setDuration(timeSource * 1000);
        mColorAnimation.addUpdateListener(new ValueAnimator.AnimatorUpdateListener() {

            @Override
            public void onAnimationUpdate(ValueAnimator animator) {
                llTimerProgressbar.setBackgroundColor((Integer) animator.getAnimatedValue());
                tvTimerClock.setTextColor((Integer) animator.getAnimatedValue());
            }

        });
        mColorAnimation.start();

        llTimerProgressbar.setVisibility(View.VISIBLE);
        ObjectAnimator timerProgressBarAnimation = ObjectAnimator.ofFloat(llTimerProgressbar, "scaleX", 1f, 0f);
        llTimerProgressbar.setPivotX(0);
        timerProgressBarAnimation.setDuration(timeSource * 1000);
        timerProgressBarAnimation.start();

        if (word.onPaused()) {
            mColorAnimation.setCurrentPlayTime(mCurrentPlayTime);
            timerProgressBarAnimation.setCurrentPlayTime(mCurrentPlayTime);
        }

        showClock();
    }

    class TimerAsyncTask extends AsyncTask<Void, Void, Void> {

        @Override
        protected Void doInBackground(Void... params) {
            try {
                showTime();
                for (; 1 <= timeRemaining; timeRemaining--) {
                    Thread.sleep(1000);
                    showTime();
                }
            } catch (InterruptedException e) {
                e.printStackTrace();
            }
            return null;
        }

        @Override
        protected void onPostExecute(Void aVoid) {
            super.onPostExecute(aVoid);
            hideProgressBar();
            hideClock();
            Toast.makeText(activity, activity.getString(R.string.time_is_up), Toast.LENGTH_SHORT).show();
        }
    }

    void hideClock() {
        if (timerAsyncTask.getStatus() == AsyncTask.Status.FINISHED) return;
        Animation animation = AnimationUtils.loadAnimation(activity.getApplicationContext(), R.anim.slide_to_left_from_screen);
        animation.setFillAfter(true);
        ll_timer_clock.startAnimation(animation);
    }
    void hideProgressBar() {
        llTimerProgressbar.clearAnimation();
        llTimerProgressbar.setVisibility(View.INVISIBLE);
    }
    void showClock() {
        Animation animation = AnimationUtils.loadAnimation(activity.getApplicationContext(), R.anim.slide_from_left_to_screen);
        animation.setFillAfter(true);
        ll_timer_clock.startAnimation(animation);
    }
}
