package com.blank_paper.game.leap_frog.fragments;

import android.app.Fragment;
import android.os.Bundle;
import android.support.annotation.Nullable;
import android.text.Html;
import android.text.method.LinkMovementMethod;
import android.text.style.URLSpan;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.view.animation.Animation;
import android.view.animation.AnimationUtils;
import android.view.animation.ScaleAnimation;
import android.view.animation.TranslateAnimation;
import android.webkit.WebView;
import android.widget.TextView;
import com.blank_paper.game.leap_frog.utils.UserAvgLevelCalculator;
import com.blank_paper.game.leap_frog.MainActivity;
import com.blank_paper.game.leap_frog.R;
import com.blank_paper.game.leap_frog.utils.DbHelper;

/**
 * Created by walkmn on 24.07.15.
 */
public class MenuFragment extends Fragment {

    private View viewProgressGreen, ivMind, viewProgressBackground;
    private boolean hasWidth; // so bad
    private MainActivity mActivity;

    @Nullable
    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container, Bundle savedInstanceState) {
        View rootView = inflater.inflate(R.layout.fragment_menu, container, false);

        mActivity = (MainActivity) getActivity();
        viewProgressGreen = rootView.findViewById(R.id.viewProgressGreen);
        viewProgressBackground = rootView.findViewById(R.id.viewProgressBackground);
        ivMind = rootView.findViewById(R.id.ivMind);

        rootView.findViewById(R.id.bt_start).setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                mActivity.startGame();
            }
        });
        rootView.findViewById(R.id.bt_exit).setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                mActivity.onBackPressed();
            }
        });
        rootView.findViewById(R.id.tvTitle).startAnimation(AnimationUtils.loadAnimation(getActivity(), R.anim.slide_out_bottom));

        DbHelper dbHelper = ((MainActivity) getActivity()).getDbHelper();
        setProgress((float) dbHelper.getSolvedWordCount() / dbHelper.getAllWordCount());

        ((TextView) rootView.findViewById(R.id.tv_menu_lvl_completed)).setText(String.valueOf(dbHelper.getSolvedWordCount()));
        ((TextView) rootView.findViewById(R.id.tv_user_avg_lvl)).setText(new UserAvgLevelCalculator(dbHelper).getAvgUserLevel());

        ((TextView) rootView.findViewById(R.id.link)).setMovementMethod(LinkMovementMethod.getInstance());

        return rootView;
    }

    private void setProgress(final float percent) {
        hasWidth = false;
        viewProgressBackground.addOnLayoutChangeListener(new View.OnLayoutChangeListener() {
            public void onLayoutChange(View v, int left, int top, int right, int bottom,
                                       int oldLeft, int oldTop, int oldRight, int oldBottom) {
                if (hasWidth) return;
                int width = right - left;
                startProgressAnimation(percent, width);
                hasWidth = true;
            }
        });
    }

    private void startProgressAnimation(float percentPro, int width) {

        int duration = (int) (2000 * percentPro);

        TranslateAnimation a = new TranslateAnimation(
                Animation.ABSOLUTE, 0, Animation.ABSOLUTE, width * percentPro,
                Animation.ABSOLUTE, 0, Animation.ABSOLUTE, 0);
        a.setDuration(duration);
        a.setFillAfter(true); //HERE
        ivMind.startAnimation(a);

        ScaleAnimation animScale = new ScaleAnimation(0f, percentPro, // Start and end values for the X axis scaling
                1f, 1f, // Start and end values for the Y axis scaling
                Animation.RELATIVE_TO_SELF, 0f, // Pivot point of X scaling
                Animation.RELATIVE_TO_SELF, 0f); // Pivot point of Y scaling)
        animScale.setFillAfter(true);
        animScale.setFillBefore(true);
        animScale.setDuration(duration);
        viewProgressGreen.startAnimation(animScale);
    }
}