package com.blank_paper.game.leap_frog.utils;

import android.content.Context;
import android.content.SharedPreferences;

import com.blank_paper.game.leap_frog.object.UserLocalData;

/**
 * Created by walkmn on 30.07.15.
 */
public class StorageManager {

    private Context context;
    private static String USER_DATA = "USER_DATA";

    public StorageManager(Context context) {
        this.context = context;
    }

    public UserLocalData restoreUserData() {
        UserLocalData userData = new UserLocalData();
        SharedPreferences settings = context.getSharedPreferences(USER_DATA, 0);
        //userData.setLevel(settings.getInt("user_lvl", 0));
        userData.setGold(settings.getInt("user_gold", 0));
        return userData;
    }

    public void saveUserData(UserLocalData userData) {
        SharedPreferences settings = context.getSharedPreferences(USER_DATA, 0);
        SharedPreferences.Editor editor = settings.edit();
        //editor.putInt("user_lvl", userData.getLevel());
        editor.putInt("user_gold", userData.getGold());
        editor.apply();
    }

    public void saveGold(int gold) {
        SharedPreferences settings = context.getSharedPreferences(USER_DATA, 0);
        SharedPreferences.Editor editor = settings.edit();
        editor.putInt("user_gold", gold);
        editor.apply();
    }

    public int getGold() {
        SharedPreferences settings = context.getSharedPreferences(USER_DATA, 0);
        return settings.getInt("user_gold", 0);
    }

    public void openNewDb() {
        SharedPreferences settings = context.getSharedPreferences(USER_DATA, 0);
        SharedPreferences.Editor editor = settings.edit();
        editor.putBoolean("db_isNewVersion", true);
        editor.apply();
    }
    public void closeNewDb() {
        SharedPreferences settings = context.getSharedPreferences(USER_DATA, 0);
        SharedPreferences.Editor editor = settings.edit();
        editor.putBoolean("db_isNewVersion", false);
        editor.apply();
    }
    public boolean getDataBaseParam() {
        SharedPreferences settings = context.getSharedPreferences(USER_DATA, 0);
        return settings.getBoolean("db_isNewVersion", false);
    }
    // for timer
    public void saveTimeRemaining(int timeRemaining) {
        SharedPreferences settings = context.getSharedPreferences(USER_DATA, 0);
        SharedPreferences.Editor editor = settings.edit();
        editor.putInt("timer_remainingtime", timeRemaining);
        editor.apply();
    }
    public int getTimeRemaining() {
        SharedPreferences settings = context.getSharedPreferences(USER_DATA, 0);
        return settings.getInt("timer_remainingtime", 0);
    }

    public void saveCurrentPlayTime(long currPlayTime) {
        SharedPreferences settings = context.getSharedPreferences(USER_DATA, 0);
        SharedPreferences.Editor editor = settings.edit();
        editor.putLong("timer_curr_play_time", currPlayTime);
        editor.apply();
    }
    public long getCurrentPlayTime() {
        SharedPreferences settings = context.getSharedPreferences(USER_DATA, 0);
        return settings.getLong("timer_curr_play_time", 0);
    }

    // for timer
    public void saveActiveGameTime(int time) {
        SharedPreferences settings = context.getSharedPreferences(USER_DATA, 0);
        SharedPreferences.Editor editor = settings.edit();
        editor.putInt("active_time", time);
        editor.apply();
    }
    public int getActiveGameTime() {
        SharedPreferences settings = context.getSharedPreferences(USER_DATA, 0);
        return settings.getInt("active_time", 0);
    }
    public void saveGiftGold(int gold) {
        SharedPreferences settings = context.getSharedPreferences(USER_DATA, 0);
        SharedPreferences.Editor editor = settings.edit();
        editor.putInt("gift_gold", gold);
        editor.apply();
    }
    public int getGiftGold() {
        SharedPreferences settings = context.getSharedPreferences(USER_DATA, 0);
        return settings.getInt("gift_gold", 0);
    }

    public boolean hasGift() {
        if (getGiftGold() == 0) {
            return false;
        }
        return true;
    }

    public void openGift() {
        saveGiftGold(0);
    }

    public void gameCenterConected() {
        SharedPreferences settings = context.getSharedPreferences(USER_DATA, 0);
        SharedPreferences.Editor editor = settings.edit();
        editor.putBoolean("gc_connected", true);
        editor.apply();
    }
    public boolean isGameCenterConnected() {
        SharedPreferences settings = context.getSharedPreferences(USER_DATA, 0);
        return settings.getBoolean("gc_connected", false);
    }
}
