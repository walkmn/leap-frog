package com.blank_paper.game.leap_frog.message_box;

import android.app.Activity;
import android.app.Dialog;
import android.content.DialogInterface;
import android.view.View;
import android.view.animation.Animation;
import android.view.animation.AnimationUtils;
import android.widget.Button;
import android.widget.ImageView;
import android.widget.TextView;
import com.blank_paper.game.leap_frog.OnChangeGameListener;
import com.blank_paper.game.leap_frog.R;
import com.blank_paper.game.leap_frog.object.Word;

/**
 * Created by walkmn on 27.07.15.
 */
public class FinishLvlMessageBox {

    private static final int STAR_RAITING_0_COST = 2;
    private static final int STAR_RAITING_1_COST = 4;
    private static final int STAR_RAITING_2_COST = 8;
    private static final int STAR_RAITING_3_COST = 12;

    private Activity activity;
    private Word word;
    private Dialog dialog;
    private OnChangeGameListener onChangeGameListener;

    private Button btLike, btFinish;
    private TextView tvGold, tvWord;
    private ImageView ivStar1, ivStar2, ivStar3;
    private int lvlRating = 0;

    public FinishLvlMessageBox(final Activity activity, final Word word) {
        this.activity = activity;
        this.word = word;
        if (word.getSolvedTime() < (word.getSuperTime() * 3)) {
            lvlRating = 1;
        }
        if (word.getSolvedTime() < (word.getSuperTime() * 2)) {
            lvlRating = 2;
        }
        if (word.getSolvedTime() < word.getSuperTime()) {
            lvlRating = 3;
        }

        dialog = new Dialog(activity, android.R.style.Theme_Translucent_NoTitleBar);
        dialog.getWindow().getAttributes().windowAnimations = R.style.dialog_animation;
        dialog.setOnDismissListener(new DialogInterface.OnDismissListener() {
            @Override
            public void onDismiss(DialogInterface dialogInterface) {
                dialog = null;
            }
        });
        dialog.setCancelable(false);
        dialog.setContentView(R.layout.dialog_finish_lvl);

        btLike = (Button) dialog.findViewById(R.id.bt_finish_lvl_like);
        btFinish = (Button) dialog.findViewById(R.id.bt_finish_ok);
        tvGold = (TextView) dialog.findViewById(R.id.tv_finish_lvl_gold);
        tvWord = (TextView) dialog.findViewById(R.id.tv_finish_word);
        ivStar1 = (ImageView) dialog.findViewById(R.id.iv_finish_lvl_raiting1);
        ivStar2 = (ImageView) dialog.findViewById(R.id.iv_finish_lvl_raiting2);
        ivStar3 = (ImageView) dialog.findViewById(R.id.iv_finish_lvl_raiting3);

        btFinish.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                btFinish.setEnabled(false);
                if (onChangeGameListener != null) onChangeGameListener.onLevelFinished(word);
                btLike.setVisibility(View.INVISIBLE);
                Animation animation = AnimationUtils.loadAnimation(activity.getApplicationContext(), R.anim.slide_in_bottom);
                animation.setAnimationListener(new Animation.AnimationListener() {
                    @Override
                    public void onAnimationStart(Animation animation) {
                    }

                    @Override
                    public void onAnimationEnd(Animation animation) {
                        dialog.dismiss();
                    }

                    @Override
                    public void onAnimationRepeat(Animation animation) {
                    }
                });
                dialog.findViewById(R.id.rl_dialog_finish).startAnimation(animation);
            }
        });
        btLike.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {

            }
        });

        btFinish.setVisibility(View.INVISIBLE);
        tvGold.setVisibility(View.INVISIBLE);
    }

    public void show() {
        if (activity != null) {
            activity.runOnUiThread(new Runnable() {
                public void run() {
                    ((ImageView) dialog.findViewById(R.id.iv_word_image)).setImageDrawable(word.getImageDrawable());
                    tvWord.setText(word.getWord());
                    int gold = STAR_RAITING_0_COST;
                    switch (lvlRating) {
                        case 1 : gold = STAR_RAITING_1_COST; break;
                        case 2 : gold = STAR_RAITING_2_COST; break;
                        case 3 : gold = STAR_RAITING_3_COST; break;
                    }
                    tvGold.setText("+" + gold);
                    tvWord.setText(word.getWord());
                    dialog.show();
                    addAllAnimation();
                    if (onChangeGameListener != null) onChangeGameListener.onGetGold(gold);
                }
            });
        }
    }

    private void addAllAnimation() {
        Animation animation = AnimationUtils.loadAnimation(activity.getApplicationContext(), R.anim.slide_out_bottom);
        animation.setAnimationListener(new Animation.AnimationListener() {
            @Override
            public void onAnimationStart(Animation animation) {
            }

            @Override
            public void onAnimationEnd(Animation animation) {
                Animation animationWord = AnimationUtils.loadAnimation(activity.getApplicationContext(), R.anim.word);
                tvWord.startAnimation(animationWord);
                animationWord.setAnimationListener(new Animation.AnimationListener() {
                    @Override
                    public void onAnimationStart(Animation animation) {
                    }

                    @Override
                    public void onAnimationEnd(Animation animation) {
                        setRatingStars();
                    }

                    @Override
                    public void onAnimationRepeat(Animation animation) {
                    }
                });
            }

            @Override
            public void onAnimationRepeat(Animation animation) {
            }
        });
        dialog.findViewById(R.id.rl_dialog_finish).startAnimation(animation);
    }

    private void setRatingStars() {
        if (lvlRating > 0) {
            ivStar1.setImageDrawable(activity.getResources().getDrawable(R.mipmap.raiting_on));
            final Animation animationStar1= AnimationUtils.loadAnimation(activity.getApplicationContext(), R.anim.star);
            animationStar1.setAnimationListener(new Animation.AnimationListener() {
                @Override
                public void onAnimationStart(Animation animation) {
                }

                @Override
                public void onAnimationEnd(Animation animation) {
                    if (lvlRating > 1) {
                        ivStar2.setImageDrawable(activity.getResources().getDrawable(R.mipmap.raiting_on));
                        final Animation animationStar2= AnimationUtils.loadAnimation(activity.getApplicationContext(), R.anim.star);
                        animationStar2.setAnimationListener(new Animation.AnimationListener() {
                            @Override
                            public void onAnimationStart(Animation animation) {
                            }

                            @Override
                            public void onAnimationEnd(Animation animation) {
                                if (lvlRating > 2) {
                                    ivStar3.setImageDrawable(activity.getResources().getDrawable(R.mipmap.raiting_on));
                                    final Animation animationStar3= AnimationUtils.loadAnimation(activity.getApplicationContext(), R.anim.star);
                                    animationStar3.setAnimationListener(new Animation.AnimationListener() {
                                        @Override
                                        public void onAnimationStart(Animation animation) {
                                        }

                                        @Override
                                        public void onAnimationEnd(Animation animation) {
                                            onLoadAllStars();
                                        }

                                        @Override
                                        public void onAnimationRepeat(Animation animation) {
                                        }
                                    });
                                    ivStar3.startAnimation(animationStar3);
                                } else {
                                    onLoadAllStars();
                                }
                            }

                            @Override
                            public void onAnimationRepeat(Animation animation) {
                            }
                        });
                        ivStar2.startAnimation(animationStar2);
                    } else {
                        onLoadAllStars();
                    }
                }

                @Override
                public void onAnimationRepeat(Animation animation) {
                }
            });
            ivStar1.startAnimation(animationStar1);
        } else {
            onLoadAllStars();
        }
    }

    private void onLoadAllStars() {
        tvGold.setVisibility(View.VISIBLE);
        Animation animationGold = AnimationUtils.loadAnimation(activity.getApplicationContext(), R.anim.slide_gold);
        animationGold.setAnimationListener(new Animation.AnimationListener() {
            @Override
            public void onAnimationStart(Animation animation) {
            }

            @Override
            public void onAnimationEnd(Animation animation) {
                if (onChangeGameListener != null) onChangeGameListener.onAnimationFinishedGetGold();
            }

            @Override
            public void onAnimationRepeat(Animation animation) {
            }
        });
        tvGold.startAnimation(animationGold);

        if (lvlRating == 3) {
            btLike.setVisibility(View.VISIBLE);
            Animation animationLike = AnimationUtils.loadAnimation(activity.getApplicationContext(), R.anim.like);
            btLike.startAnimation(animationLike);
        }

        btFinish.setVisibility(View.VISIBLE);
        Animation animationBtFinish = AnimationUtils.loadAnimation(activity.getApplicationContext(), R.anim.star);
        btFinish.startAnimation(animationBtFinish);
    }

    public FinishLvlMessageBox setOnLvlCompleteClickListener(OnChangeGameListener onClickListener) {
        this.onChangeGameListener = onClickListener;
        return this;
    }
}
