package com.blank_paper.game.leap_frog;

import android.app.AlarmManager;
import android.app.PendingIntent;
import android.content.BroadcastReceiver;
import android.content.Context;
import android.content.Intent;
import android.os.Build;

import com.blank_paper.game.leap_frog.utils.DbHelper;

import java.util.Calendar;

public class GifterReceiver extends BroadcastReceiver {

    @Override
    public void onReceive(Context context, Intent intent) {

        if (Intent.ACTION_BOOT_COMPLETED.equals(intent.getAction())) {
            GifterReceiver.scheduleServiceUpdates(context);
        } else {

            if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.O) {
                context.startForegroundService(new Intent(context, MainService.class));
            } else {
                context.startService(new Intent(context, MainService.class));
            }
        }
    }

    /**
     * Schedule the next update
     *
     * @param context
     *            the current application context
     */
    public static void scheduleServiceUpdates(final Context context) {

        DbHelper dbHelper = new DbHelper(context);
        if (dbHelper.isGameOver()) {
            return;
        }

        final Intent intent = new Intent(context, GifterReceiver.class);

        // create intent for our alarm receiver (or update it if it exists)
        /*if ((PendingIntent.getBroadcast(context, 0, intent, PendingIntent.FLAG_NO_CREATE) != null)) {
            return;
        }*/

        final PendingIntent pending = PendingIntent.getBroadcast(context, 0, intent, PendingIntent.FLAG_CANCEL_CURRENT);

        // compute first call time 1 minute from now
        Calendar calendar = Calendar.getInstance();
        calendar.add(Calendar.HOUR, 20);
        long trigger = calendar.getTimeInMillis();

        // set delay between each call : 24 Hours
        long delay = 20 * 60 * 60 * 1000;
        //long delay = 15 * 1000;

        // Set alarm
        AlarmManager alarmManager = (AlarmManager) context.getSystemService(Context.ALARM_SERVICE);
        alarmManager.setRepeating(AlarmManager.RTC, trigger, delay, pending);
        // you can use RTC_WAKEUP instead of RTC to wake up the device
    }
}
