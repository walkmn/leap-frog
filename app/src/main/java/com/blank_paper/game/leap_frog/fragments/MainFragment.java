package com.blank_paper.game.leap_frog.fragments;

import android.app.Fragment;
import android.os.Bundle;
import android.support.annotation.Nullable;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.view.animation.Animation;
import android.view.animation.AnimationUtils;
import android.widget.Button;

import com.blank_paper.game.leap_frog.MainApplication;
import com.blank_paper.game.leap_frog.utils.StatisticSender;
import com.blank_paper.game.leap_frog.OnChangeGameListener;
import com.blank_paper.game.leap_frog.controllers.GameController;
import com.blank_paper.game.leap_frog.MainActivity;
import com.blank_paper.game.leap_frog.R;
import com.blank_paper.game.leap_frog.message_box.FinishGameMessageBox;
import com.blank_paper.game.leap_frog.object.UserLocalData;
import com.blank_paper.game.leap_frog.object.Word;
import com.blank_paper.game.leap_frog.utils.BuyActionType;
import com.blank_paper.game.leap_frog.utils.DbHelper;
import com.blank_paper.game.leap_frog.utils.WordsArrayMixer;

import java.util.ArrayList;

/**
 * Created by walkmn on 24.07.15.
 */
public class MainFragment extends Fragment implements OnChangeGameListener {

    private MainActivity mActivity;

    private ArrayList<Word> words;
    private UserLocalData userLocalData;
    private GameController gameController;

    private Button tvGameGold, btBack;
    private DbHelper dbHelper;
    private Word currentWord;
    private int currentLvl = 0;
    //private AdView mAdView;

    @Nullable
    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container, Bundle savedInstanceState) {
        View rootView = inflater.inflate(R.layout.fragment_main, container, false);

        MainApplication application = (MainApplication) getActivity().getApplication();

        btBack = (Button) rootView.findViewById(R.id.bt_main_back);
        tvGameGold = (Button) rootView.findViewById(R.id.bt_game_gold);
        //mAdView = (AdView) rootView.findViewById(R.id.adView);

        btBack.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                ((MainActivity) getActivity()).gotoMenu();
            }
        });

        userLocalData = new UserLocalData(getActivity());

        gameController = new GameController(rootView, this);
        gameController.setOnChangeGameListener(this);

        mActivity = (MainActivity) getActivity();
        dbHelper = mActivity.getDbHelper();
        words = dbHelper.getNotSolvedWords();
        words = new WordsArrayMixer(words).mixWords();

        /*if (words.size() == 0) {
            btBack.performClick();
            return rootView;
        }*/
        currentWord = words.get(currentLvl);
        gameController.playWord(currentWord);

        updateGoldView();

        return rootView;
    }

    @Override
    public void onDestroy() {
        if (dbHelper != null && currentWord != null) dbHelper.pauseWord(currentWord);
        gameController.stopGame();
        super.onDestroy();
    }

    @Override
    public void onGetGold(int gold) {
        userLocalData.addGold(gold);
        userLocalData.save();
    }

    @Override
    public void onTakeGold(BuyActionType buyActionType, int gold) {
        userLocalData.takeGold(gold);
        userLocalData.save();
        updateGoldView();
        switch (buyActionType) {
            case BUY_IMAGE: dbHelper.buyWordImage(currentWord); break;
            case BUY_LETTER: dbHelper.saveWordOpenedLetter(gameController.getCurrentWord());   break;
        }
    }

    @Override
    public void onAnimationFinishedGetGold() {
        updateGoldView();
    }

    @Override
    public void onLevelFinished(Word word) {
        dbHelper.solveWord(word);
        currentLvl++;
        if (words.size() < dbHelper.getSolvedWordCount()) {
            gameFinished();
            return;
        }
        currentWord = words.get(currentLvl);
        gameController.playWord(currentWord);
        updateGoldView();
    }

    private void updateGoldView() {
        Animation anim = AnimationUtils.loadAnimation(getActivity().getApplicationContext(), R.anim.add_gold);
        tvGameGold.startAnimation(anim);
        tvGameGold.setText(String.valueOf(userLocalData.getGold()));
    }

    public int getGold() {
        return userLocalData.getGold();
    }

    private void gameFinished() {
        new FinishGameMessageBox(getActivity()).show();
        btBack.performClick();
    }
}
