package com.blank_paper.game.leap_frog;

import com.blank_paper.game.leap_frog.object.Word;
import com.blank_paper.game.leap_frog.utils.BuyActionType;

/**
 * Created by walkmn on 27.07.15.
 */
public interface OnChangeGameListener {
    void onGetGold(int gold);
    void onTakeGold(BuyActionType buyActionType, int gold);
    void onAnimationFinishedGetGold();
    void onLevelFinished(Word word);
}
