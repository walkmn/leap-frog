package com.blank_paper.game.leap_frog.utils;

/**
 * Created by walkmn on 03.08.15.
 */
public enum BuyActionType {
    BUY_LETTER, BUY_IMAGE
}
