package com.blank_paper.game.leap_frog.utils;

import android.content.ContentValues;
import android.content.Context;
import android.database.Cursor;
import android.database.sqlite.SQLiteDatabase;
import android.database.sqlite.SQLiteOpenHelper;
import android.util.Log;

import com.blank_paper.game.leap_frog.object.Word;

import java.util.ArrayList;

/**
 * Created by walkmn on 28.07.15.
 *
 * http://hmkcode.com/android-simple-sqlite-database-tutorial/
 *
 */
public class DbHelper {

    private static final String TABLE_WORDS = "words";
    private static final String KEY_WORD = "word";
    private static final String KEY_SOLVED = "solved";
    private static final String TIME_REMAINING = "timeRemaining";
    private static final String KEY_BUY_IMAGE = "buy_image";
    private static final String KEY_OPENED_LETTER = "opened_letter";

    private Context context;
    private SQLiteDatabase db, dbFromAsset;
    private AssetDatabaseOpenHelper assetDatabaseOpenHelper;
    private DBHelper dbHelper;

    public DbHelper(Context context) {
        this.context = context;
        dbHelper = new DBHelper(context);
        assetDatabaseOpenHelper = new AssetDatabaseOpenHelper(this.context);
        dbFromAsset = assetDatabaseOpenHelper.openDatabase();
        db = dbHelper.getWritableDatabase();
        if (assetDatabaseOpenHelper.isNewVersionDb()) {
            syncDataBases();
            assetDatabaseOpenHelper.installDbCompleted();
        }
    }

    public ArrayList<Word> getAllWords() {

        ArrayList<Word> words = new ArrayList<>();

        Cursor  cursor = db.rawQuery("select * from " + TABLE_WORDS, null);

        if (cursor .moveToFirst()) {
            do {
                words.add(new Word(cursor));
            } while (cursor.moveToNext());
        }

        return words;
    }

    private void syncDataBases() {
        ArrayList<Word> words = new ArrayList<>();
        ArrayList<Word> wordsAsset = new ArrayList<>();

        Cursor  cursor = db.rawQuery("select * from " + TABLE_WORDS, null);
        Cursor  cursorAsset = dbFromAsset.rawQuery("select * from " + TABLE_WORDS, null);

        int gg = 0;
        if (cursorAsset.moveToFirst()) {
            do {
                wordsAsset.add(new Word(cursorAsset));
                gg++;
                Log.e("gg", "" + gg);
            } while (cursorAsset.moveToNext());
        }
        Log.e("wordsAsset size", "" + wordsAsset.size());
        if (cursor.moveToFirst()) {
            while (cursor.moveToNext()) {
                words.add(new Word(cursor));
            }
        }
        Log.e("words size", "" + words.size());

        while (words.size() > 0) {
            for (int j = 0; j < wordsAsset.size() - 1; j++) {
                if (wordsAsset.get(j).getWord().contentEquals(words.get(0).getWord())) {
                    wordsAsset.remove(0);
                    words.remove(0);
                }
            }
        }

        for (int i = 0; i < wordsAsset.size(); i++) {
            db.insert(TABLE_WORDS, null, wordsAsset.get(i).getContentValues());
        }
    }

    public boolean isGameOver() {
        return getNotSolvedWords().size() == 0;
    }

    public ArrayList<Word> getNotSolvedWords() {

        ArrayList<Word> words = new ArrayList<>();

        Cursor  cursor = db.rawQuery("select * from " + TABLE_WORDS + " WHERE " + KEY_SOLVED + " == 0 OR " + KEY_SOLVED + " == -1", null);

        if (cursor .moveToFirst()) {
            while (cursor.moveToNext()) {
                words.add(new Word(cursor));
            }
        }

        return words;
    }

    public ArrayList<Word> getSolvedWords() {

        ArrayList<Word> words = new ArrayList<>();

        Cursor  cursor = db.rawQuery("select * from " + TABLE_WORDS + " WHERE " + KEY_SOLVED + " > 0", null);

        if (cursor .moveToFirst()) {
            while (cursor.moveToNext()) {
                words.add(new Word(cursor));
            }
        }

        return words;
    }

    public int solveWord(Word word) {

        ContentValues values = new ContentValues();
        values.put(KEY_SOLVED, word.getSolvedTime());

        // updating row
        return db.update(TABLE_WORDS, values, KEY_WORD + " = '" + word.getWord() + "'", null);
    }

    public int buyWordImage(Word word) {

        ContentValues values = new ContentValues();
        values.put(KEY_BUY_IMAGE, 1);

        // updating row
        return db.update(TABLE_WORDS, values, KEY_WORD + " = '" + word.getWord() + "'", null);
    }

    public int saveWordOpenedLetter(Word word) {

        ContentValues values = new ContentValues();
        values.put(KEY_OPENED_LETTER, word.getOpenLetters());

        // updating row
        return db.update(TABLE_WORDS, values, KEY_WORD + " = '" + word.getWord() + "'", null);
    }

    public int pauseWord(Word word) {

        //if (word.getSolvedTime() != 0) return 0;

        ContentValues values = new ContentValues();
        values.put(KEY_SOLVED, -1);
        values.put(TIME_REMAINING, word.getTimeRemaining());

        // updating row
        return db.update(TABLE_WORDS, values, KEY_WORD + " = '" + word.getWord() + "'", null);
    }

    public int getSolvedWordCount() { // TODO solved != -1
        String countQuery = "SELECT * FROM " + TABLE_WORDS + " WHERE " + KEY_SOLVED + " != 0 AND " + KEY_SOLVED + " != -1";
        Cursor cursor = db.rawQuery(countQuery, null);
        int count = cursor.getCount();
        cursor.close();
        return count;
    }

    public int getAllWordCount() {
        String countQuery = "SELECT * FROM " + TABLE_WORDS;
        Cursor cursor = db.rawQuery(countQuery, null);
        int count = cursor.getCount();
        cursor.close();
        return count;
    }

    public Word getCurrentWord() {
        Cursor  cursor = db.rawQuery("select * from " + TABLE_WORDS + " WHERE " + KEY_SOLVED + " == -1", null);

        if (cursor .moveToFirst()) {
            return new Word(cursor);
        }

        return null;
    }

    class DBHelper extends SQLiteOpenHelper {

        public DBHelper(Context context) {
            super(context, "wordLocalStorageDb", null, 1);
        }

        @Override
        public void onCreate(SQLiteDatabase db) {
            db.execSQL("CREATE TABLE \"words\" (`_id` INTEGER NOT NULL PRIMARY KEY AUTOINCREMENT UNIQUE, `word` INTEGER NOT NULL UNIQUE, `image` INTEGER NOT NULL UNIQUE, `super_time` INTEGER, `timeRemaining` INTEGER DEFAULT 0, `solved` INTEGER DEFAULT 0, `buy_image` INTEGER DEFAULT 0, `opened_letter` TEXT)");
        }

        @Override
        public void onUpgrade(SQLiteDatabase db, int oldVersion, int newVersion) {

        }
    }
}
