package com.blank_paper.game.leap_frog;

import android.app.Activity;
import android.app.AlertDialog;
import android.app.FragmentManager;
import android.app.FragmentTransaction;
import android.content.DialogInterface;
import android.os.Bundle;
import com.blank_paper.game.leap_frog.utils.StorageManager;
import com.blank_paper.game.leap_frog.fragments.MainFragment;
import com.blank_paper.game.leap_frog.fragments.MenuFragment;
import com.blank_paper.game.leap_frog.message_box.FinishGameMessageBox;
import com.blank_paper.game.leap_frog.utils.DbHelper;

public class MainActivity extends Activity {

    private FragmentManager fragmentManager;
    private DbHelper dbHelper;

    private StorageManager storageManager;

    public static final String AUTO_START_GAME_KEY = "autoStartGame";

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);

        // Obtain the shared Tracker instance.
        MainApplication application = (MainApplication) getApplication();

        storageManager = new StorageManager(this);

        fragmentManager = getFragmentManager();
        dbHelper = new DbHelper(this);

        gotoMenu();

        if (getIntent() != null && getIntent().getBooleanExtra(AUTO_START_GAME_KEY, false)) {
            startGame();
        }
    }

    @Override
    protected void onPause() {
        super.onPause();
        gotoMenu();
    }

    @Override
    protected void onStop() {
        GifterReceiver.scheduleServiceUpdates(this);
        super.onStop();
    }

    public void startGame() {

        if (dbHelper.isGameOver()) {
            new FinishGameMessageBox(this).show();
            return;
        }
        fragmentManager
                .beginTransaction()
                .setCustomAnimations(R.anim.slide_in_left, R.anim.slide_in_right)
                .replace(R.id.fragment_container, new MainFragment(), "game")
                .commit();
    }

    public void gotoMenu() {
        if (fragmentManager.findFragmentByTag("menu") != null) {
            return;
        }
        fragmentManager
                .beginTransaction()
                .setCustomAnimations(R.anim.activity_down_up_enter, R.anim.slide_in_right)
                .replace(R.id.fragment_container, new MenuFragment(), "menu")
                .commit();
    }

    @Override
    public void onBackPressed() {
        new AlertDialog.Builder(MainActivity.this)
                .setMessage(R.string.exit_question)
                .setPositiveButton(getString(R.string.yes), new DialogInterface.OnClickListener() {
                    @Override
                    public void onClick(DialogInterface dialog, int which) {
                        finish();
                    }
                })
                .setNegativeButton(getString(R.string.no), new DialogInterface.OnClickListener() {
                    @Override
                    public void onClick(DialogInterface dialog, int which) {
                    }
                })
                .create()
                .show();
    }

    public DbHelper getDbHelper() {
        return dbHelper;
    }
}
