package com.blank_paper.game.leap_frog.message_box;

import android.app.Activity;
import android.app.Dialog;
import android.content.DialogInterface;
import android.content.Intent;
import android.net.Uri;
import android.view.View;
import android.view.animation.Animation;
import android.view.animation.AnimationUtils;
import android.widget.Button;
import android.widget.TextView;

import com.blank_paper.game.leap_frog.R;

/**
 * Created by walkmn on 27.07.15.
 */
public class FinishGameMessageBox {

    private Activity activity;
    private Dialog dialog;
    private TextView tvHint, tvWinGame;
    private Button btMakeComment, btClose;

    public FinishGameMessageBox(final Activity activity) {
        this.activity = activity;

        dialog = new Dialog(activity, android.R.style.Theme_Translucent_NoTitleBar);
        dialog.getWindow().getAttributes().windowAnimations = R.style.dialog_animation;
        dialog.setOnDismissListener(new DialogInterface.OnDismissListener() {
            @Override
            public void onDismiss(DialogInterface dialogInterface) {
                dialog = null;
            }
        });
        dialog.setCancelable(true);
        dialog.setContentView(R.layout.dialog_finish_game);

        tvHint = (TextView) dialog.findViewById(R.id.tv_hint);
        tvWinGame = (TextView) dialog.findViewById(R.id.tvWinGame);
        btMakeComment = (Button) dialog.findViewById(R.id.bt_make_comment);
        btClose = (Button) dialog.findViewById(R.id.bt_close);

        tvHint.setVisibility(View.INVISIBLE);
        btMakeComment.setVisibility(View.INVISIBLE);
        btClose.setVisibility(View.INVISIBLE);
        tvWinGame.setVisibility(View.INVISIBLE);

        btClose.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                dialog.dismiss();
            }
        });
        btMakeComment.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                final String appPackageName = activity.getPackageName(); // getPackageName() from Context or Activity object
                try {
                    activity.startActivity(new Intent(Intent.ACTION_VIEW, Uri.parse("market://details?id=" + appPackageName)));
                } catch (android.content.ActivityNotFoundException anfe) {
                    activity.startActivity(new Intent(Intent.ACTION_VIEW, Uri.parse("http://play.google.com/store/apps/details?id=" + appPackageName)));
                }
                dialog.dismiss();
            }
        });
    }

    public void show() {
        if (activity != null) {
            activity.runOnUiThread(new Runnable() {
                public void run() {
                    addAllAnimation();
                    dialog.show();
                }
            });
        }
    }

    private void addAllAnimation() {
        Animation animation = AnimationUtils.loadAnimation(activity.getApplicationContext(), R.anim.slide_out_bottom);
        animation.setAnimationListener(new Animation.AnimationListener() {
            @Override
            public void onAnimationStart(Animation animation) { }

            @Override
            public void onAnimationEnd(Animation animation) {
                Animation animationWinGame = AnimationUtils.loadAnimation(activity.getApplicationContext(), R.anim.word);
                tvWinGame.startAnimation(animationWinGame);
                tvWinGame.setVisibility(View.VISIBLE);
                animationWinGame.setAnimationListener(new Animation.AnimationListener() {
                    @Override
                    public void onAnimationStart(Animation animation) { }

                    @Override
                    public void onAnimationEnd(Animation animation) {
                        Animation animationWord = AnimationUtils.loadAnimation(activity.getApplicationContext(), R.anim.button_next);
                        animationWord.setStartOffset(0);
                        btMakeComment.startAnimation(animationWord);
                        btMakeComment.setVisibility(View.VISIBLE);
                        animationWord.setAnimationListener(new Animation.AnimationListener() {
                            @Override
                            public void onAnimationStart(Animation animation) { }

                            @Override
                            public void onAnimationEnd(Animation animation) {
                                Animation animationWord = AnimationUtils.loadAnimation(activity.getApplicationContext(), R.anim.alpha_in);
                                tvHint.setVisibility(View.VISIBLE);
                                tvHint.startAnimation(animationWord);
                                animationWord.setDuration(800);
                                animationWord.setAnimationListener(new Animation.AnimationListener() {
                                    @Override
                                    public void onAnimationStart(Animation animation) { }

                                    @Override
                                    public void onAnimationEnd(Animation animation) {
                                        btClose.setVisibility(View.VISIBLE);
                                        Animation anim = AnimationUtils.loadAnimation(activity.getApplicationContext(), R.anim.button_next);
                                        anim.setStartOffset(200);
                                        btClose.startAnimation(anim);
                                    }

                                    @Override
                                    public void onAnimationRepeat(Animation animation) { }
                                });
                            }

                            @Override
                            public void onAnimationRepeat(Animation animation) { }
                        });
                    }

                    @Override
                    public void onAnimationRepeat(Animation animation) { }
                });
            }

            @Override
            public void onAnimationRepeat(Animation animation) { }
        });
        dialog.findViewById(R.id.rl_dialog_finish).startAnimation(animation);
    }
}
