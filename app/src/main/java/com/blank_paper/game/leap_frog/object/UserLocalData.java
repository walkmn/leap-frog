package com.blank_paper.game.leap_frog.object;

import android.content.Context;

import com.blank_paper.game.leap_frog.utils.StorageManager;

/**
 * Created by walkmn on 30.07.15.
 */
public class UserLocalData {

    private StorageManager storageManager;

    private int gold;

    public int getGold() {
        return gold;
    }

    public void setGold(int gold) {
        this.gold = gold;
    }
    public void addGold(int gold) {
        this.gold += gold;
    }

    public UserLocalData(Context context) {
        storageManager = new StorageManager(context);

        // restore
        gold = storageManager.getGold();
    }

    public void save() {
        storageManager.saveUserData(this);
    }

    public UserLocalData() {
    }

    public void takeGold(int gold) {
        this.gold -= gold;
    }
}
