package com.blank_paper.game.leap_frog.controllers;

import android.app.Activity;
import android.app.AlertDialog;
import android.content.DialogInterface;
import android.graphics.drawable.Drawable;
import android.view.View;
import android.view.animation.Animation;
import android.view.animation.AnimationUtils;
import android.widget.Button;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.Toast;
import com.blank_paper.game.leap_frog.OnChangeGameListener;
import com.blank_paper.game.leap_frog.message_box.FinishLvlMessageBox;
import com.blank_paper.game.leap_frog.R;
import com.blank_paper.game.leap_frog.fragments.MainFragment;
import com.blank_paper.game.leap_frog.object.Word;
import com.blank_paper.game.leap_frog.utils.BuyActionType;
import com.blank_paper.game.leap_frog.utils.PhraseGenerator;
import com.blank_paper.game.leap_frog.utils.RandomChar;
import com.blank_paper.game.leap_frog.utils.WordMixer;
import java.io.IOException;
import java.util.ArrayList;

/**
 * Created by walkmn on 25.07.15.
 */
public class GameController {

    private static final int IMAGE_COST = 50;
    private static final int LETTER_COST = 20;

    private final TimerController timerController;
    private GiftController giftController;
    private Button btClean;
    private ImageView ivWordImage;

    private Activity activity;
    private LinearLayout llLetterContainer;
    private LinearLayout llLetterPlaceContainer;
    private Word word;
    private PhraseGenerator phraseGenerator;
    private MainFragment mainFragment;

    public Word getCurrentWord() {
        return word;
    }
    public void setOnChangeGameListener(OnChangeGameListener onChangeGameListener) {
        this.onChangeGameListener = onChangeGameListener;
    }

    private OnChangeGameListener onChangeGameListener;

    private ArrayList<Button> letters = new ArrayList<>();
    private ArrayList<Button> letterPlaces = new ArrayList<>();

    private String sourceWord;

    public GameController(View view, MainFragment mainFragment) {
        this.mainFragment = mainFragment;
        llLetterContainer = (LinearLayout) view.findViewById(R.id.ll_later_container);
        llLetterPlaceContainer = (LinearLayout) view.findViewById(R.id.ll_later_place_container);
        btClean = (Button) view.findViewById(R.id.bt_clean);
        ivWordImage = (ImageView) view.findViewById(R.id.iv_word_image);
        this.activity = mainFragment.getActivity();

        timerController = new TimerController(view, activity);
        giftController = new GiftController(view, mainFragment);
    }

    public void playWord(final Word word) {
        this.word = word;
        this.sourceWord = word.getWord().toLowerCase();
        try {
            word.setImageDrawable(Drawable.createFromStream(activity.getAssets().open(word.getImage() + ".png"), null)); // fetch
        } catch (IOException e) {
            e.printStackTrace();
        }
        clearField();

        if (word.isBuyImage()) {
            showWordImage();
        } else {
            noWordImage();
        }

        String playWord = new WordMixer().mix(sourceWord);

        for (int i = 0; i < playWord.length(); i++) {
            letters.add(addLetter(playWord.charAt(i)));
            addPlace();
        }
        addBuyLetterButton();
        btClean.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                for (Button bt : letterPlaces) {
                    bt.performClick();
                }
            }
        });

        timerController.start(word);
        openLetters();
    }

    private void openLetters() {
        for (char letter : word.getOpenLetters().toCharArray()) {
            buyLetter(letter);
        }
    }

    private void buyLetter(char letter) {
        for (Button letterBtn : letters) { // hide in bottom panel
            if (letterBtn.getText().charAt(0) == letter) {
                letterBtn.setVisibility(View.INVISIBLE);
                break;
            }
        }

        for (int i = 0; i < word.getWord().length(); i++) {
            if (word.getWord().charAt(i) == letter) {
                letterPlaces.get(i).setBackgroundResource(R.mipmap.bt_main_letter_place_buy);
                letterPlaces.get(i).setOnClickListener(null);
                letterPlaces.get(i).setText(letter + "");
                break;
            }
        }
    }

    public void stopGame() {
        timerController.stop();
        giftController.stop();
    }

    private void clearField() {
        llLetterContainer.removeAllViews();
        llLetterPlaceContainer.removeAllViews();
        btClean.setVisibility(View.INVISIBLE);

        letters.clear();
        letterPlaces.clear();
        phraseGenerator = null;
    }

    private void noWordImage() {
        ivWordImage.setImageResource(R.mipmap.question);
        final Animation anim = AnimationUtils.loadAnimation(activity.getApplicationContext(), R.anim.add_gold);
        anim.setStartOffset(4000);
        anim.setAnimationListener(new Animation.AnimationListener() {
            @Override
            public void onAnimationStart(Animation animation) {
            }

            @Override
            public void onAnimationEnd(Animation animation) {
                ivWordImage.startAnimation(anim);
            }

            @Override
            public void onAnimationRepeat(Animation animation) {
            }
        });
        ivWordImage.startAnimation(anim);
        ivWordImage.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                if (word.isBuyImage()) return;
                new AlertDialog.Builder(activity)
                        .setMessage(activity.getString(R.string.question_buy_image) + IMAGE_COST + activity.getString(R.string.gold))
                        .setPositiveButton(activity.getString(R.string.yes), new DialogInterface.OnClickListener() {
                            @Override
                            public void onClick(DialogInterface dialog, int which) {
                                if (mainFragment.getGold() < IMAGE_COST) {
                                    Toast.makeText(activity, R.string.no_money, Toast.LENGTH_SHORT).show();
                                    return;
                                }
                                if (onChangeGameListener != null)
                                    onChangeGameListener.onTakeGold(BuyActionType.BUY_IMAGE, IMAGE_COST);
                                showWordImage();
                            }
                        })
                        .setNegativeButton(activity.getString(R.string.no), new DialogInterface.OnClickListener() {
                            @Override
                            public void onClick(DialogInterface dialog, int which) {
                            }
                        })
                        .create()
                        .show();
            }
        });
    }
    private void showWordImage() {
        Animation animation = AnimationUtils.loadAnimation(activity.getApplicationContext(), R.anim.show_image_part1);
        if (word.isBuyImage()) animation.setDuration(0);
        animation.setAnimationListener(new Animation.AnimationListener() {
            @Override
            public void onAnimationStart(Animation animation) {
            }

            @Override
            public void onAnimationEnd(Animation animation) {
                ivWordImage.setImageDrawable(word.getImageDrawable());
                Animation animationWord = AnimationUtils.loadAnimation(activity.getApplicationContext(), R.anim.show_image_part2);
                ivWordImage.startAnimation(animationWord);
            }

            @Override
            public void onAnimationRepeat(Animation animation) {
            }
        });
        ivWordImage.startAnimation(animation);
        ivWordImage.setOnClickListener(null);
    }

    private Button addLetter(char letter) {
        final Button letterView = (Button) activity.getLayoutInflater().inflate(R.layout.item_letter, llLetterContainer, false);

        letterView.setText(String.valueOf(letter));
        letterView.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                hideClearButton();
                letterView.setVisibility(View.INVISIBLE);
                addLetterToPlace(letterView);
            }
        });

        llLetterContainer.addView(letterView);

        return letterView;
    }

    private void addBuyLetterButton() {
        final Button letterView = (Button) activity.getLayoutInflater().inflate(R.layout.item_letter_buy, llLetterContainer, false);
        letterView.setText("?");
        letterView.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                if (word.getOpenLetters().length() == word.getWord().length() - 1) return;
                new AlertDialog.Builder(activity)
                        .setMessage(activity.getString(R.string.question_buy_letter) + LETTER_COST + activity.getString(R.string.gold))
                        .setPositiveButton(activity.getString(R.string.yes), new DialogInterface.OnClickListener() {
                            @Override
                            public void onClick(DialogInterface dialog, int which) {
                                if (mainFragment.getGold() < LETTER_COST) {
                                    Toast.makeText(activity, R.string.no_money, Toast.LENGTH_SHORT).show();
                                    return;
                                }
                                btClean.performClick();
                                char randChar = RandomChar.getFrom(word);
                                word.addOpenLetter(randChar);
                                buyLetter(randChar);
                                if (onChangeGameListener != null)
                                    onChangeGameListener.onTakeGold(BuyActionType.BUY_LETTER, LETTER_COST);
                            }
                        })
                        .setNegativeButton(activity.getString(R.string.no), null)
                        .create()
                        .show();
            }
        });
        llLetterContainer.addView(letterView);
    }

    private void addPlace() {
        final Button letterPlaceView = (Button) activity.getLayoutInflater().inflate(R.layout.item_letter_place, llLetterPlaceContainer, false);

        letterPlaceView.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                hideClearButton();
                if (letterPlaceView.getText().length() != 0) {
                    showLetter(letterPlaceView);
                    letterPlaceView.setText(null);
                }
            }
        });
        llLetterPlaceContainer.addView(letterPlaceView);
        letterPlaces.add(letterPlaceView);
    }

    private void addLetterToPlace(Button letterBtn) {

        for (Button button : letterPlaces) {
            if (button.getText().length() == 0) {
                button.setText(letterBtn.getText());
                break;
            }
        }

        StringBuilder stringBuilder = new StringBuilder();
        for (Button button : letterPlaces) {
            stringBuilder.append(button.getText().toString());
        }
        if (stringBuilder.toString().length() != sourceWord.length()) return;
        if (stringBuilder.toString().contentEquals(sourceWord)) {
            timerController.stop();
            word.setSolvedTime(timerController.getSolvedTime());
            word.setTimeRemaining(timerController.getTimeRemaining());
            new FinishLvlMessageBox(activity, word).setOnLvlCompleteClickListener(onChangeGameListener).show();
        } else {
            btClean.setVisibility(View.VISIBLE);
            btClean.startAnimation(AnimationUtils.loadAnimation(activity, R.anim.alpha_in));
            if (phraseGenerator == null) {
                phraseGenerator = new PhraseGenerator(word);
            }
            Toast.makeText(activity, phraseGenerator.generate(), Toast.LENGTH_SHORT).show();
        }
    }

    private void hideClearButton() {
        if (btClean.getVisibility() == View.INVISIBLE) return;
        Animation anim = AnimationUtils.loadAnimation(activity, R.anim.alpha_out);
        anim.setAnimationListener(new Animation.AnimationListener() {
            @Override
            public void onAnimationStart(Animation animation) {
            }

            @Override
            public void onAnimationEnd(Animation animation) {
                btClean.setVisibility(View.INVISIBLE);
            }

            @Override
            public void onAnimationRepeat(Animation animation) {
            }
        });
        btClean.startAnimation(anim);
    }

    private void showLetter(Button letterBtn) {
        for (Button button : letters) {
            if (button.getText().toString().charAt(0) == letterBtn.getText().charAt(0) && button.getVisibility() == View.INVISIBLE) {
                button.setVisibility(View.VISIBLE);
                break;
            }
        }
    }
}
