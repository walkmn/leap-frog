package com.blank_paper.game.leap_frog.message_box;

import android.app.Activity;
import android.app.Dialog;
import android.content.DialogInterface;
import android.view.View;
import android.view.animation.Animation;
import android.view.animation.AnimationUtils;
import android.widget.Button;
import android.widget.RelativeLayout;
import android.widget.TextView;

import com.blank_paper.game.leap_frog.OnChangeGameListener;
import com.blank_paper.game.leap_frog.R;

import java.util.ArrayList;

/**
 * Created by walkmn on 27.07.15.
 */
public class GiftMessageBox {

    private Activity activity;
    private Dialog dialog;

    private RelativeLayout rlMain;
    private TextView tvGiftMoney;
    private Button btGiftOk;
    private ArrayList<OnChangeGameListener> finishListener = new ArrayList<>();

    public GiftMessageBox(final Activity activity, final int gold) {
        this.activity = activity;
        dialog = new Dialog(activity, android.R.style.Theme_Translucent_NoTitleBar);
        dialog.getWindow().getAttributes().windowAnimations = R.style.dialog_animation;
        dialog.setOnDismissListener(new DialogInterface.OnDismissListener() {
            @Override
            public void onDismiss(DialogInterface dialogInterface) {
            }
        });
        dialog.setCancelable(false);
        dialog.setContentView(R.layout.dialog_gift);

        rlMain = (RelativeLayout) dialog.findViewById(R.id.rl_gift_main);
        tvGiftMoney = (TextView) dialog.findViewById(R.id.tv_gift_money);
        btGiftOk = (Button) dialog.findViewById(R.id.bt_gift_ok);

        tvGiftMoney.setVisibility(View.INVISIBLE);
        btGiftOk.setVisibility(View.INVISIBLE);

        btGiftOk.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                btGiftOk.setEnabled(false);

                for (OnChangeGameListener onChangeGameListener : finishListener) onChangeGameListener.onGetGold(gold);

                Animation animation = AnimationUtils.loadAnimation(activity.getApplicationContext(), R.anim.slide_in_bottom);
                animation.setAnimationListener(new Animation.AnimationListener() {
                    @Override
                    public void onAnimationStart(Animation animation) {
                    }

                    @Override
                    public void onAnimationEnd(Animation animation) {
                        dialog.dismiss();
                    }

                    @Override
                    public void onAnimationRepeat(Animation animation) {
                    }
                });
                rlMain.startAnimation(animation);
            }
        });
        ((TextView) dialog.findViewById(R.id.tv_gift_money)).setText(gold + activity.getString(R.string.money));

    }

    public void show() {
        if (activity != null) {
            activity.runOnUiThread(new Runnable() {
                public void run() {
                    dialog.show();
                }
            });

            Animation animation = AnimationUtils.loadAnimation(activity.getApplicationContext(), R.anim.slide_out_bottom);
            animation.setAnimationListener(new Animation.AnimationListener() {
                @Override
                public void onAnimationStart(Animation animation) {
                }

                @Override
                public void onAnimationEnd(Animation animation) {
                    animateGiftGold();
                    animateButtonOk();
                }

                @Override
                public void onAnimationRepeat(Animation animation) {
                }
            });
            rlMain.startAnimation(animation);
        }
    }

    private void animateGiftGold() {
        tvGiftMoney.setVisibility(View.VISIBLE);
        Animation animation = AnimationUtils.loadAnimation(activity.getApplicationContext(), R.anim.star);
        tvGiftMoney.startAnimation(animation);
    }
    private void animateButtonOk() {
        btGiftOk.setVisibility(View.VISIBLE);
        Animation animation = AnimationUtils.loadAnimation(activity.getApplicationContext(), R.anim.button_next);
        animation.setStartOffset(450);
        animation.setAnimationListener(new Animation.AnimationListener() {
            @Override
            public void onAnimationStart(Animation animation) {
            }
            @Override
            public void onAnimationEnd(Animation animation) {
                for (OnChangeGameListener onChangeGameListener : finishListener) onChangeGameListener.onAnimationFinishedGetGold();
            }
            @Override
            public void onAnimationRepeat(Animation animation) {
            }
        });
        btGiftOk.startAnimation(animation);
    }

    public GiftMessageBox addOnFinishListener(OnChangeGameListener finishListener) {
        this.finishListener.add(finishListener);
        return this;
    }
}
