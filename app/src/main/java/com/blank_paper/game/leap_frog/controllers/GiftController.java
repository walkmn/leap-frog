package com.blank_paper.game.leap_frog.controllers;

import android.app.Activity;
import android.view.View;
import android.view.animation.Animation;
import android.view.animation.AnimationUtils;
import android.widget.Button;
import android.widget.Toast;

import com.blank_paper.game.leap_frog.message_box.GiftMessageBox;
import com.blank_paper.game.leap_frog.OnChangeGameListener;
import com.blank_paper.game.leap_frog.R;
import com.blank_paper.game.leap_frog.fragments.MainFragment;
import com.blank_paper.game.leap_frog.object.Word;
import com.blank_paper.game.leap_frog.utils.BuyActionType;
import com.blank_paper.game.leap_frog.utils.StorageManager;

import java.util.Random;
import java.util.Timer;
import java.util.TimerTask;

/**
 * Created by Andrew on 05.08.2015.
 */
public class GiftController {

    private static final int GOOD_GAME_TIME = 5 * 60; // in second - 1.5 hours
    //private static final int GOOD_GAME_TIME = 3; // in second

    private final StorageManager storageManager;
    private Timer timer;
    private int activeGameTime = 0;
    private Activity activity;

    private Button btPresent;

    public GiftController(View rootView, final MainFragment mainFragment) {
        this.activity = mainFragment.getActivity();
        storageManager = new StorageManager(activity);

        activeGameTime = storageManager.getActiveGameTime();

        btPresent = (Button) rootView.findViewById(R.id.bt_get_present);
        btPresent.setVisibility(View.GONE);

        if (storageManager.hasGift()) {
            showButton();
        } else {
            startTimer();
        }


        btPresent.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                storageManager.openGift();
                hideButton();

                int gift = getRandMoney();
                new GiftMessageBox(activity, gift)
                        .addOnFinishListener(mainFragment)
                        .addOnFinishListener(new OnChangeGameListener() {
                            @Override
                            public void onGetGold(int gold) {
                                startTimer();
                            }
                            @Override
                            public void onTakeGold(BuyActionType buyActionType, int gold) {
                            }
                            @Override
                            public void onAnimationFinishedGetGold() {
                            }
                            @Override
                            public void onLevelFinished(Word word) {
                            }
                        })
                        .show();
            }
        });
    }

    public void stop() {
        storageManager.saveActiveGameTime(activeGameTime);
        if (timer != null) {
            timer.cancel();
        }
    }

    public void addSecondForActiveTime() {
        activeGameTime += 1;

        if (activeGameTime > GOOD_GAME_TIME) {
            activeGameTime = 0;
            stop();
            presentGift();
        }
    }

    private void startTimer() {
        timer = new Timer();
        timer.schedule(new TimerTask() {
            @Override
            public void run() {
                addSecondForActiveTime();
            }
        }, 0, 1000);
    }

    private void presentGift() {
        activity.runOnUiThread(new Runnable() {
            public void run() {
                Toast.makeText(activity, R.string.get_gift, Toast.LENGTH_SHORT).show();
                showButton();
            }
        });
    }

    private void showButton() {
        btPresent.setVisibility(View.VISIBLE);
        Animation anim = AnimationUtils.loadAnimation(activity, R.anim.button_next);
        anim.setStartOffset(500);
        btPresent.startAnimation(anim);
    }
    private void hideButton() {
        Animation anim = AnimationUtils.loadAnimation(activity, R.anim.alpha_out);
        anim.setAnimationListener(new Animation.AnimationListener() {
            @Override
            public void onAnimationStart(Animation animation) {
            }

            @Override
            public void onAnimationEnd(Animation animation) {
                btPresent.setVisibility(View.GONE);
            }

            @Override
            public void onAnimationRepeat(Animation animation) {
            }
        });
        btPresent.startAnimation(anim);
    }

    public int getRandMoney() {
        int randPercent = new Random().nextInt(100);
        if (randPercent <= 50) return 15;
        if (randPercent > 60 && randPercent <= 80) return 20;
        if (randPercent > 80 && randPercent <= 90) return 30;
        if (randPercent > 90 && randPercent <= 99) return 50;
        return 10;
    }
}
