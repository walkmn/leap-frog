package com.blank_paper.game.leap_frog.utils;

import com.blank_paper.game.leap_frog.object.Word;

import java.util.ArrayList;
import java.util.Collections;

/**
 * Created by walkmn on 02.08.15.
 */
public class WordsArrayMixer {

    private ArrayList<Word> wordsInput;

    public WordsArrayMixer(ArrayList<Word> wordsInput) {
        this.wordsInput = wordsInput;
    }

    public ArrayList<Word> mixWords() {
        ArrayList<Word> result = new ArrayList<>();


        for (Word word : wordsInput) {
            if (word.getSolvedTime() == -1) {
                result.add(word);
            }
        }

        result.addAll(getWordsArrayByWordLength(3));
        result.addAll(getWordsArrayByWordLength(4));
        result.addAll(getWordsArrayByWordLength(5));
        result.addAll(getWordsArrayByWordLength(6));
        result.addAll(getWordsArrayByWordLength(7));
        result.addAll(getWordsArrayByWordLength(8));

        return result;
    }

    private  ArrayList<Word> getWordsArrayByWordLength(int wordLength) {

        ArrayList<Word> result = new ArrayList<>();

        for (Word word : wordsInput) {
            if (word.getWord().length() == wordLength && word.getSolvedTime() != -1) {
                result.add(word);
            }
        }

        Collections.shuffle(result);
        return result;
    }
}
