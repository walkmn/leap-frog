package com.blank_paper.game.leap_frog;

import android.app.Notification;
import android.app.NotificationManager;
import android.app.PendingIntent;
import android.app.Service;
import android.content.Context;
import android.content.Intent;
import android.os.Build;
import android.os.IBinder;
import android.support.v4.app.NotificationCompat;
import android.support.v4.app.TaskStackBuilder;

import com.blank_paper.game.leap_frog.object.Word;
import com.blank_paper.game.leap_frog.utils.DbHelper;
import com.blank_paper.game.leap_frog.utils.RandomChar;

import java.util.Timer;
import java.util.TimerTask;

import static com.blank_paper.game.leap_frog.MainActivity.AUTO_START_GAME_KEY;

/**
 * Created by Andrew on 05.08.2015.
 */
public class MainService extends Service {

    private DbHelper dbHelper;

    @Override
    public void onCreate() {
        super.onCreate();
        dbHelper = new DbHelper(getBaseContext());
    }

    public int onStartCommand(Intent intent, int flags, int startId) {

        if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.O) {

            Notification.Builder builder = new Notification.Builder(this, getString(R.string.notification_channel_id))
                    .setContentTitle(getString(R.string.app_name))
                    .setWhen(0)
                    .setAutoCancel(true);

            Notification notification = builder.build();
            startForeground(1, notification);
        }
        presentLetter();
        stopSelf();

        return super.onStartCommand(intent, flags, startId);
    }

    public IBinder onBind(Intent intent) {
        return null;
    }

    private void presentLetter() {
        Word word = dbHelper.getCurrentWord();
        if (word == null) {
            return;
        }
        if (word.getOpenLetters() != null) {
            if (word.getOpenLetters().length() > countGoldCharacter()) {

                /*NotificationCompat.Builder mBuilder =
                        new NotificationCompat.Builder(this)
                                .setAutoCancel(true)
                                .setSmallIcon(R.mipmap.ic_launcher)
                                .setContentTitle(getString(R.string.what_is_it));

                if (word.getOpenLetters().length() == 1) {
                    mBuilder.setContentText(getString(R.string.help_if_knowed_one_char) + word.getOpenLetters() + getString(R.string.help_if_knowed_one_char2));
                }
                if (word.getOpenLetters().length() > 1) {
                    mBuilder.setContentText(getString(R.string.help_if_knowed_any_char) + word.getOpenLetters() + getString(R.string.help_if_knowed_any_char2));
                }
                Intent openHomePageActivity = new Intent(this, MainActivity.class);
                TaskStackBuilder stackBuilder = TaskStackBuilder.create(this);
                stackBuilder.addNextIntent(openHomePageActivity);

                PendingIntent resultPendingIntent =
                        stackBuilder.getPendingIntent(
                                0,
                                PendingIntent.FLAG_ONE_SHOT
                        );
                mBuilder.setContentIntent(resultPendingIntent);
                NotificationManager mNotificationManager =
                        (NotificationManager) getSystemService(Context.NOTIFICATION_SERVICE);

                mNotificationManager.notify(0, mBuilder.build());*/

                return;
            }
        }

        char randomChar = RandomChar.getFrom(word);
        word.addOpenLetter(randomChar);
        dbHelper.saveWordOpenedLetter(word);
        NotificationCompat.Builder mBuilder =
                new NotificationCompat.Builder(this, getString(R.string.notification_channel_id))
                        .setAutoCancel(true)
                        .setSmallIcon(R.mipmap.ic_launcher)
                        .setContentTitle(getString(R.string.help));

        if (word.getOpenLetters().length() == 1) {
            mBuilder.setContentText(getString(R.string.help_if_knowed_one_char) + word.getOpenLetters() + getString(R.string.help_if_knowed_one_char2));
        }
        if (word.getOpenLetters().length() > 1) {
            mBuilder.setContentText(getString(R.string.help_if_knowed_any_char) + word.getOpenLetters() + getString(R.string.help_if_knowed_any_char2));
        }
        Intent openHomePageActivity = new Intent(this, MainActivity.class);
        openHomePageActivity.putExtra(AUTO_START_GAME_KEY, true);
        TaskStackBuilder stackBuilder = TaskStackBuilder.create(this);
        stackBuilder.addNextIntent(openHomePageActivity);

        PendingIntent resultPendingIntent =
                stackBuilder.getPendingIntent(
                        0,
                        PendingIntent.FLAG_ONE_SHOT
                );
        mBuilder.setContentIntent(resultPendingIntent);
        NotificationManager mNotificationManager =
                (NotificationManager) getSystemService(Context.NOTIFICATION_SERVICE);

        mNotificationManager.notify(0, mBuilder.build());
    }

    private int countGoldCharacter() {
        Word word = dbHelper.getCurrentWord();
        if (word != null && word.getWord() != null) {
            return word.getWord().length() / 3;
        }
        return 0;
    }
}
