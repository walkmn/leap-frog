package com.blank_paper.game.leap_frog.utils;

import com.blank_paper.game.leap_frog.object.Word;

/**
 * Created by walkmn on 02.09.15.
 */
public class StatisticSender {

    //private Tracker mTracker;
    private int gold;
    private int lvl;

    public StatisticSender() {

    }

    public void send(int gold, int lvl) {
        //if (mTracker == null) return;
        this.gold = gold;
        this.lvl = lvl;

        /*mTracker.send(new HitBuilders.EventBuilder()
                .setCategory("Stop game. User level - \"" + getLevelLvl() + "\"")
                .setAction("Gold count: " + getGoldLvl())
                .build());*/
    }

    public void send(Word word) {
        //if (mTracker == null) return;
        if (word == null) return;
        /*mTracker.send(new HitBuilders.EventBuilder()
                .setCategory("Exit")
                .setAction("Abort word: " + word.getWord())
                .build());*/
    }

    private String getGoldLvl() {
        String goldLvl = "0 .. 50";

        if (gold >= 0 && gold < 50) {
            goldLvl = "0 .. 50";
        }
        if (gold >= 50 && gold < 150) {
            goldLvl = "50 .. 150";
        }
        if (gold >= 150 && gold < 300) {
            goldLvl = "150 .. 300";
        }
        if (gold >= 300) {
            goldLvl = "300 .. ~";
        }

        return goldLvl;
    }

    private String getLevelLvl() {
        String leveLvl = "beginer";

        if (lvl >= 0 && lvl < 50) {
            leveLvl = "beginer";
        }
        if (lvl >= 50 && lvl < 100) {
            leveLvl = "student";
        }
        if (lvl >= 100 && lvl < 150) {
            leveLvl = "master";
        }
        if (lvl >= 150) {
            leveLvl = "andrew_tsymbal";
        }

        return leveLvl;
    }
}
