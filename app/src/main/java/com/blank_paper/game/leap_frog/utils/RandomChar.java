package com.blank_paper.game.leap_frog.utils;

import com.blank_paper.game.leap_frog.object.Word;

import java.util.Random;

/**
 * Created by walkmn on 06.08.15.
 */
public class RandomChar {

    public static char getFrom(Word word) {
        StringBuilder string = new StringBuilder(word.getWord());
        if (word.getOpenLetters() == null) return string.charAt(new Random().nextInt(string.length()));
        for (int i = 0; i < word.getOpenLetters().length(); i++) {
            for (int j = 0; j < string.length(); j++) {
                if (word.getOpenLetters().toCharArray()[i] == string.charAt(j)) {
                    string.deleteCharAt(j);
                    break;
                }
            }
        }
        return string.charAt(new Random().nextInt(string.length()));
    }
}
