package com.blank_paper.game.leap_frog.utils;

import java.util.Random;

/**
 * Created by walkmn on 29.07.15.
 */
public class WordMixer {

    private Random rand = new Random();

    public String mix(String sourceWord) {

        StringBuilder sourceWord_pro = new StringBuilder(sourceWord);
        StringBuilder mixedWord = new StringBuilder();


        int randomCharIndex = rand.nextInt(sourceWord_pro.length() - 1) + 1;
        mixedWord.append(sourceWord_pro.charAt(randomCharIndex));
        sourceWord_pro.deleteCharAt(randomCharIndex);

        do {
            randomCharIndex = rand.nextInt(sourceWord_pro.length());
            mixedWord.append(sourceWord_pro.charAt(randomCharIndex));
            sourceWord_pro.deleteCharAt(randomCharIndex);
        } while (sourceWord_pro.length() != 0);

        return mixedWord.toString();
    }
}
