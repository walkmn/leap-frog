package com.blank_paper.game.leap_frog.utils;

/**
 * https://gist.github.com/donaldlittlepie/1271795
 *
 * sudo add-apt-repository ppa:linuxgndu/sqlitebrowser
 * sudo apt-get update
 * sudo apt-get install sqlitebrowser
 *
 */
import java.io.File;
import java.io.FileOutputStream;
import java.io.IOException;
import java.io.InputStream;
import java.io.OutputStream;

import android.content.Context;
import android.database.sqlite.SQLiteDatabase;

public class AssetDatabaseOpenHelper {

    private static final String DB_NAME = "main.db";

    private Context context;

    public boolean isNewVersionDb() {
        return newVersionDb;
    }

    private boolean newVersionDb;
    private StorageManager storageManager;

    public AssetDatabaseOpenHelper(Context context) {
        this.context = context;
        storageManager = new StorageManager(context);
        newVersionDb = storageManager.getDataBaseParam();
    }

    public SQLiteDatabase openDatabase() {
        File dbFile = context.getDatabasePath(DB_NAME);

        if (!dbFile.exists()) {
            storageManager.openNewDb();
            try {
                SQLiteDatabase checkDB = context.openOrCreateDatabase(DB_NAME, context.MODE_PRIVATE, null);
                if(checkDB != null) {

                    checkDB.close();

                }
                copyDatabase(dbFile);
            } catch (IOException e) {
                throw new RuntimeException("Error creating source database", e);
            }
        }
        newVersionDb = storageManager.getDataBaseParam();

        return SQLiteDatabase.openDatabase(dbFile.getPath(), null, SQLiteDatabase.OPEN_READWRITE);
    }

    public void installDbCompleted() {
        storageManager.closeNewDb();
    }

    private void copyDatabase(File dbFile) throws IOException {
        InputStream is = context.getAssets().open(DB_NAME);
        OutputStream os = new FileOutputStream(dbFile);

        byte[] buffer = new byte[1024];
        while (is.read(buffer) > 0) {
            os.write(buffer);
        }

        os.flush();
        os.close();
        is.close();
    }

}
