package com.blank_paper.game.leap_frog.object;

import android.content.ContentValues;
import android.database.Cursor;
import android.graphics.drawable.Drawable;

/**
 * Created by walkmn on 28.07.15.
 */
public class Word {

    private int timeRemaining = 0;

    public String getOpenLetters() {
        return openLetters;
    }

    private String openLetters;

    public String getWord() {
        return word;
    }

    public String getImage() {
        return image;
    }

    public int getSuperTime() {
        return superTime;
    }

    public Drawable getImageDrawable() {
        return imageDrawable;
    }

    public int getSolvedTime() {
        return solvedTime;
    }

    public boolean onPaused() {
        if (solvedTime == -1) return true;
        return false;
    }

    public void setSolvedTime(int solvedTime) {
        this.solvedTime = solvedTime;
    }

    public boolean isBuyImage() {
        return buyImage;
    }

    public void setImageDrawable(Drawable imageDrawable) {
        this.imageDrawable = imageDrawable;
    }

    private String word;
    private String image;
    private Drawable imageDrawable;
    private int superTime;
    private int solvedTime;
    private boolean buyImage;

    public Word(Cursor cursor) {
        this.word = cursor.getString(1);
        this.image = cursor.getString(2);
        this.superTime = cursor.getInt(3);
        this.timeRemaining = cursor.getInt(4);
        this.solvedTime = cursor.getInt(5);
        this.buyImage = cursor.getInt(6) != 0;
        this.openLetters = cursor.getString(7);
        if (openLetters == null) openLetters = "";
    }

    public Word(String word, String image, int superTime, int timeRemaining, int solvedTime, boolean buyImage, String openLetters) {
        this.word = word;
        this.image = image;
        this.superTime = superTime;
        this.timeRemaining = timeRemaining;
        this.solvedTime = solvedTime;
        this.buyImage = buyImage;
        this.openLetters = openLetters;
    }

    public void setTimeRemaining(int timeRemaining) {
        this.timeRemaining = timeRemaining;
    }

    public int getTimeRemaining() {
        return timeRemaining;
    }

    public void addOpenLetter(char letter) {
        openLetters = openLetters + letter;
    }

    public ContentValues getContentValues() {
        ContentValues cv = new ContentValues();

        cv.put("word", word);
        cv.put("image", image);
        cv.put("super_time", superTime);
        cv.put("timeRemaining", timeRemaining);
        cv.put("solved", solvedTime);
        cv.put("buy_image", buyImage ? 1 : 0);

        return cv;
    }
}
