package com.blank_paper.game.leap_frog.utils;

import android.util.Log;

import com.blank_paper.game.leap_frog.object.Word;

import java.util.ArrayList;

/**
 * Created by walkmn on 25.09.15.
 */
public class UserAvgLevelCalculator {

    private final ArrayList<Word> solvedWords;

    public UserAvgLevelCalculator(DbHelper dbHelper) {
        solvedWords = dbHelper.getSolvedWords();
    }

    public String getAvgUserLevel() {
        return String.format("%.1f",(1.00 - calculate()) * 10);
    }

    private float calculate() {
        int maxTime = 0, remainingTime = 0;
        for (int i = 0; i < solvedWords.size(); i++) {
            maxTime += (solvedWords.get(i).getSuperTime() * 3);
            remainingTime += solvedWords.get(i).getSolvedTime();

            Log.e(i + " maxTime: ", solvedWords.get(i).getSuperTime() + "");
            Log.e(i + " remainingTime: ",solvedWords.get(i).getSolvedTime() + "");
            Log.e("---", "---");
        }
        Log.e("---", "---");
        Log.e("res maxTime: ",maxTime + "");
        Log.e("res remainingTime: ",remainingTime + "");

        return maxTime == 0 ? maxTime : ((float) remainingTime / (float) maxTime);
    }
}
